module.exports = {
  title: 'Notas',
  base: '/notes/',
  description: '',
  dest: 'public',
  themeConfig: {
    smothScroll: false,
    displayAllHeaders: false,
    nav: [
      { text: 'Inicio', link: '/' },
      { text: 'Git', link: '/git/' },
    ],
    sidebar: [
      '/', {
        collapsable: true,
        sidebarDepth: 2,
        title: "Control de versiones",
        path: "/git/",
        children: ["/git/",
                  "/git/commit",
                  "/git/pullrequest",
                  "/git/branching",
                  "/git/tipsandtricks"]
      },
      {
        collapsable: true,
        sidebarDepth: 2,
        title: "Artefactos",
        path: "/artefactos/",
        children: ["/artefactos/"]
      },
      {
        collapsable: true,
        sidebarDepth: 2,
        title: "Diagramas",
        path: "/diagrams/",
        children: ["/diagrams/siac",
                  "/diagrams/BI2",]
      },
      {
        collapsable: true,
        sidebarDepth: 2,
        title: "Proyectos",
        path: "/projects/",
        children: ["/projects/sistema_unico_informacion",
                  ]
      },
    ]
  },
  markdown: {
    extendMarkdown: md => {
      md.set({ html: true })
      md.use(require('markdown-it-katex'))
      md.use(require('markdown-it-plantuml'))
      md.use(require('markdown-it-task-lists'))
      md.use(require('markdown-it-fontawesome'))
    }
  },
  head: [
      ['link', { rel: 'stylesheet',
          href: 'https://cdn.jsdelivr.net/npm/katex@0.6.0/dist/katex.min.css' }],
      ['link', { rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.0.13/css/all.css',
        integrity: 'sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp',
        crossorigin: 'anonymous'}]
      ],
  plugins: [
    [
      'vuepress-plugin-zooming',
      {
        selector: 'img',
        delay: 1000,
        options: {
          bgColor: 'white',
          zIndex: 10000,
        },
      },
    ],
  ],
}
