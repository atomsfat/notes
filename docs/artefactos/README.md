## Versionamiento (semver)

El propósito del versionamiento es tener certeza, del impacto que la nuevas características introducen en el software.

La convención es muy simple: **{major}.{minor}.{patch}-{tag}+{buildmetadata}**

* {mayor} incremental sí el release incluye cambios que rompen la retrocompatibilidad
* {minor} incremental sí el release contiene solamente cambios que no rompen la retrocompatibilidad
* {patch} contiene corrección de errores
* {tag} opcional denota el estado donde se encuentra la versión que se pretende liberar (Snapshot/QA/RC)

**Solamente un numero debe de ser incrementado por liberacion, y los otros debe de ser puesto a cero.**

## Ten en mente

* Siempre se empieza en 0.1.0
* Antes de la versión 1.0.0 todo es desarrollo
* Si el software esta corriendo o tiene dependencia con otros la versión es **1.0.0**
* Antes de la versión 1.0.0 no debes de tener miedo de romper cosas, por que es donde estamos en modo desarrollo, cuando se tenga la versión 1.0.0 entonces sera considerado un desarrollo estable.
* Ejemplos de vambios que aumentan la versión major:
  * Cambiar la versión de JAVA/Python
  * Modificar la DB
  * Actualizar la versión de las dependencias(framework)
  * Modificar reglas de negocio

### Ejemplos:

2. Empezar a trabajar en un nuevo proyecto **0.1.0**
  * Todos los cambios antes de poner en producción aumentan la versión minor.
  * Al momento de liberar el branch de release **1.0.0-RC**
  * Si hay bugs a corregir **1.0.1-RC**
  * Al liberar se tendrá la versión estable **1.0.0**
1. Versión productiva **1.0.0**
  * Merge de cambio que necesita modificar DB a branch release TAG con **2.0.0-RC**
  * Error detectado y corregido en la versión en UAT **2.0.1-RC**
  * UAT aceptado la versión cambia a **2.0.0**.


## Referencias

[Semver](https://gitversion.readthedocs.io/en/latest/reference/intro-to-semver/)
gitversion](https://gitversion.readthedocs.io/en/latest/reference/intro-to-semver/#intro-to-semver)
