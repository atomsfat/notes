# BI Propuesta de solucion

@startuml

interface http
package "Bussines intelligence" {

node Dashboards {
  component PEFTablero [
    <b> Python Dash
    ---
    Indicadores:
      * PEF


  ]
  }


database datamarts [
  <b> Datamart PSQL
]

node "Ingest webservice"{
  component PEF [
     <b> REST werservice
     ---
     Endpoints:
      * PEF
  ]
  component WEB [
    <b> Webform
  ]
  }
[PEFTablero] -->[datamarts]: TCP:5432
[PEF] -->[datamarts]: TCP:5432
[PEF] <..[http]: HTTP:80
[WEB] <..[http]: HTTP:80
[WEB] -->[PEF]
}

package "SCT ENV" {

database "psql-1"
database "medpre-1"
database "flota-1"
database "citas-1"
node "Pentaho tools" {
  component "Data Studio(PDI)"
  component "Schema Workbench(PSW)"

}

[psql-1] -->[Data Studio(PDI)]
[medpre-1] -->[Data Studio(PDI)]
[flota-1] -->[Data Studio(PDI)]
[citas-1] -->[Data Studio(PDI)]
[Data Studio(PDI)] ..>[http]: HTTP:80
}

@enduml
