# Sample component diagram


@startuml
scale .8
title Something <&heart>💩💩💩
caption figure 1
package "Some **S** Group" {
HTTP - [First Component]
[Another Component]
}

node "10.0.1.10/24" {


  FTP - [Second Component]
  [First Component] --> FTP

  package "server1"{
   [siac]

  }
  package "server2"{
    database db1 #Yellow [
      <b>some
      ---
      * 12.10.10.0
      * WE
    ]
    db1 ~~> [siac]: TCP:4000
  }
}


@enduml
