# SIAC DIAGRAM


@startuml

package "Developer tools" {
[Git Lab]
}

interface  http 

package "Production" {

component "10.33.146.141 , 139" {
		node CISWS  [
			<b> CISWS
		    ___
		    Port
		    * 7010
		    * 7020
	    ]


	}

	node "10.33.146.43 , 42" {
		component weblogic {
		node siacserver #gray [
			<b> siac
		    ---
		    * puerto 7010
		    * puerto 7020
	    ]


	}	

	}
	note left
	 weblogic 11
	 java 1.6
	 end note
 
	database siacdb [
		<b>  SIAC
		----
		IP :
			* 10.33.141.129
			* 10.33.141.130
		====
		PORT :
			* 50000
]
	database admsegdb [
		<b>  AdmSegdb
		----
		IP :
			* 10.33.141.127 
		====
		PORTS :
			*50000
]
	database ingpbadb [
		<b>  IngPba
		----
		IP :
			* 10.33.141.129
		====
		PORTS :
			* 50000
]

[Git Lab] -->[siacserver]:http :443
siacdb ~~> [siacserver]: TCP:50000
admsegdb ~~> [siacserver]: TCP:50000
ingpbadb ~~> [siacserver]: TCP:50000
[NAS] -->[siacserver]
[CISWS] ->[siacserver]:http
[http] -->[CISWS]:http
}



package "qa" {
	node "??? " {
		component weblogicqa {
		node siacserverqa #gray [
			<b> siac
		    ---
		    * puerto 7010
		    * puerto 7020
	    ]


	}	


	}
	note left
	 weblogic 11
	 java 1.6
	 end note
 
	database siacdbqa [
		<b>  SIAC
		----
		IP :
			* 10.38.10.8
		====
		PORT :
			* 50000
]
	database admsegdbqa [
		<b>  AdmSegdb
		----
		IP :
			* 10.38.10.8
		====
		PORTS :
			*50000
]
	database ingpbadbqa [
		<b>  IngPba
		----
		IP :
			* 10.38.10.8
		====
		PORTS :
			* 50000
]

[Git Lab] -->[siacserverqa]:http :443
siacdbqa ~~> [siacserverqa]: TCP:50000
admsegdbqa ~~> [siacserverqa]: TCP:50000
ingpbadbqa ~~> [siacserverqa]: TCP:50000
}


package "DEV" {
	node "local" {
		component weblogicdev {
		node siacserverdev #gray [
			<b> siac
		    
	    ]


	}	


 	}
	note left
	 weblogic 11
	 java 1.6
	 end note
 
	database siacdbdev [
		<b>  SIAC dev
		----
		IP :
			* ?.?.?.253
		====
		PORT :
			* 50000
]
	database admsegdbdev [
		<b>  AdmSegdb dev
		----
		IP :
			* ?.?.?.253
		====
		PORTS :
			*50000
]
	database ingpbadbdev [
		<b>  IngPba dev
		----
		IP :
			* ?.?.?.253
		====
		PORTS :
			* 50000
]

[Git Lab] -->[siacserverdev]:http :443
siacdbdev ~~> [siacserverdev]: TCP:50000
admsegdbdev ~~> [siacserverdev]: TCP:50000
ingpbadbdev ~~> [siacserverdev]: TCP:50000
}


@enduml

 