# Git branching model.

Existen diferentes estrategias y flujos de trabajo que se puede implementar al utlizar Git, las mas comunes:

* [Git-flow](https://nvie.com/posts/a-successful-git-branching-model/) - Sí tienes multiples versiones en produccion. El motivo principal es soportar versiones previas en produccion mientras se desarrolla una nueva. Por ejemplo; Sistemas operativos, Paquetes de oficina, etc.

* [Github-Flow](https://guides.github.com/introduction/flow/) - Si solamanente tienes o debes de tener una version productiva en todos tus servidores.  Una vez el desarrollador termina un feature o una bug inmediatamente es promovido a una version productiva.

* [Gitlab-flow](https://about.gitlab.com/blog/2014/09/29/gitlab-flow/) - Si solamente tienes o debes de tener una version productiva en todos tus servidores, y es un software muy complicado.

## Nosotros preferimos Gitlab-flow



**Maten las cosas simples (KISS)**

1. Master(Desplegado en staging)
1. Stage es la siguiente version
1. Feature/Bug branch salen de la Stage

### Referencias

- [GithubFlow](https://guides.github.com/introduction/flow/)
- [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
- [GitFlow Vs GithubFlow](https://stackoverflow.com/questions/18188492/what-are-the-pros-and-cons-of-git-flow-vs-github-flow)
- [Atlassian simple git](https://www.atlassian.com/git/articles/simple-git-workflow-is-simple)
