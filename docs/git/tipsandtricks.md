# Git tips and tricks

*New branch*
`git checkout -b <new-branch>`

*Delete a local branch*
`git branch -d <local-branch>`

*Delete remote branch*
```
git branch -D bug fix
git push origin :the_remote_branch
git delete-branch integration
```

*Add file*
`git add <file>`

*Undo local change*
`git reset <ref>`

*Config default editor*
`git config --global core.editor "nvim"`


*Merge*
`git merge --no-ff feature/payment-definitions`

*Remove from stage*
`git reset HEAD filename`

*Undo commit*
`git reset --soft HEAD~1`



*Working with remotes*
```
Add remote
git remote add origin https://github.com/user/repo.git
```

*Pushing to remote*
```
git push  <REMOTENAME> <BRANCHNAME>
git push atoms HEAD:master
```

*New branch From stash*
`git stash branch <branchname> [<stash>]`


*List branch ordered by recent commit*
`git for-each-ref --sort=-committerdate refs/heads/`



*Diff file between branches*
`git diff --name-only master..migrationBranch | grep .*.java | tr "\n" " " | xargs checkstyle -c rulesets/google_checks.xml check style`

*Push TAGS*
```
git push origin --tags
git tag -d tagName
git push origin <tagname>
```

*Workflow*
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

