# Sistema único de información (SUI)

---
## Pantallas realizadas con Pentaho User Console

![PEF_SCT_V2](./images/PEF_SCT_V2.png)


<collapse hidden title="Sql">

<<< @/docs/projects/snippets/pef_sct_v2.sql

</collapse>


```python
query = select * from table where Id_Fecha = "" and Id_DestinoGasto = ""
df = pd.read_sql_query(query)

grouped = df.groupby('Descripcion').agg(

  'Original'  = SUM(IMP_Original_Anual)
  'Modificado' = SUM((IMP_Modificado_Anual)
  'Comprometido' = SUM((IMP_Contratado_Anual)
  '%' = (SUM((IMP_Contratado_Anual) )) /  (SUM((IMP_Modificado_Anual) ))) *  100, 0)
  'Por Comprometer' = SUM((IMP_Modificado_Anual - IMP_Contratado_Anual)
  '%' = (SUM((IMP_Modificado_Anual - IMP_Contratado_Anual) )) /  (SUM((IMP_Modificado_Anual) ))) *  100, 0)
  'Pagado' = SUM((IMP_Ejercido_Anual + IMP_Pagosiaff_Anual)
  '%' = (SUM((IMP_Ejercido_Anual + IMP_Pagosiaff_Anual) )) /  (SUM((IMP_Modificado_Anual) ))) *  100, 0)
  'Por Pagar' = SUM((IMP_Contratado_Anual - (IMP_Ejercido_Anual + IMP_Pagosiaff_Anual))
  '%' = (SUM((IMP_Contratado_Anual - (IMP_Ejercido_Anual + IMP_Pagosiaff_Anual)) )) /  (SUM((IMP_Modificado_Anual) ) ))) *  100, 0)
)
```

![PEF_SCT_V2_detalle](./images/PEF_SCT_V2_detalle.png)

<collapse hidden title="Sql">

<<< @/docs/projects/snippets/pef_sct_v2_b.sql

</collapse>

__Detalle PEF__
![PEF_SCT_V2_detalle_dos](./images/PEF_SCT_V2_detalle_dos.png)

<collapse hidden title="Sql">

<<< @/docs/projects/snippets/pef_sct_seguimiento.sql

</collapse>

![PEF_SCT_V2_detalle_tres](./images/PEF_SCT_V2_detalle_tres.png)

<collapse hidden title="Sql">

<<< @/docs/projects/snippets/pef_sct_seguimiento_detalle.sql

</collapse>

![PEF_SCT_V2_detalle_cuatro](./images/PEF_SCT_V2_detalle_cuatro.png)

<collapse hidden title="Sql">

<<< @/docs/projects/snippets/pef_sct_seguimiento_ficha.sql

</collapse>

---

__Diagrama SQL Server__

* [Diccionario de datos](/bi/Diccionario_de_datos_BD_PRESUPUESTO.xlsx)
* ![Diagrama SQL SERVER](./images/DiagramaSqlServer.png)
---
__Diagrama propuesto PSQL__

__Diagrama propuesto PSQL__


![modelo-ref](./images/modelo-ref.png)

