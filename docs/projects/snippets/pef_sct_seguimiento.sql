SELECT

      fac.ID_Unidad_Adm_Responsable as 'Cve',
      fac.Id_Fecha,
  case when UA.Descripcion_Corta is null then 'TOTAL' else  UA.Descripcion_Corta end Coordinación,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                    AS 'Original     MDP' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual + IMP_Modificado_Tramite_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado   MDP',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Comprometido_Anual + IMP_Comp_Tramite_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Comprometido MDP',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Anual + IMP_Ejercido_Tramite_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Ejercido SCT MDP',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Pagosiaff_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                   AS 'Pagado SHCP  MDP',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Disponible_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                  AS 'Disponible   MDP',
          '<button class="btn btn-sm btn-round btn-primary">Detalle</button>' as ' '



  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
       INNER JOIN [DM_PresupuestoN].[dbo].[Dim_Unidad_Administrativa] UA on
                 fac.ID_Unidad_Adm_Responsable = UA.ID_Unidad_Administrativa
  where NOT([ID_Clasificacion_Gasto]) like '4%' and
        not([ID_Unidad_Administrativa]) like '2%' and
        [ID_TipoGasto] >= 4 and
        [ID_Activ_AI] = 3 and
        [Id_Fecha] = REPLACE(${filterFecha},'-','')

    GROUP BY (UA.Descripcion_Corta),
    fac.ID_Unidad_Adm_Responsable ,
    fac.Id_Fecha
