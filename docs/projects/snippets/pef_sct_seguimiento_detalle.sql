SELECT ID_Unidad_Adm_Responsable as ' ',
       dobr.ID_Obra as 'Num Obra',

       /* DOBR.Descripcion as 'Descripcion Obra',  */
       fobr.imp_meta as 'Meta KM',
       '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contrato + IMP_IVA_Contrato) / 1000000 )  AS MONEY), 1)) as 'Inversión Total MDP',
       '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contrato + IMP_IVA_Contrato) / 1000000 )  AS MONEY), 1)) as 'Inversión en el Año MDP',
       '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Tramite / 1000000)  AS MONEY), 1))  as 'Inversión Comprometida MDP',
       '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Pagado / 1000000)  AS MONEY), 1))    as 'Inversión Ejercida SCT MDP',
       [IMP_Avance_Fisico] as 'Avance Físico %',
       case when IMP_Pagado = 0 then null else ((((IMP_Contrato + IMP_IVA_Contrato) / 1000000 ) * 100) / (IMP_Pagado / 1000000)) / 100 end as 'Avance Financiero %',
       [FechaInicioReal] as 'Fecha de Inicio',
       [FechaTerminoReal] as 'Fecha de Termino',
       '<button class="btn btn-sm btn-primary btn-round">Fciha Técnica</button>' as ' '



        FROM [DM_Obras].[dbo].[Fac_Obras] fobr
       inner join [DM_Obras].[dbo].[Dim_Obra] DOBR on fobr.ID_Obra = DOBR.ID_Obra

        where ID_Unidad_Adm_Responsable = ${region}


        group by dobr.ID_Obra,
        /* DOBR.Descripcion, */
        fobr.imp_meta,
        importe,
          IMP_Contrato,
          IMP_IVA_Contrato,
          IMP_Pagado,
          fobr.IMP_Asignacion,
          [IMP_Avance_Fisico],
          [IMP_Reduccion],
          [IMP_Total],
          [FechaInicioReal],
       [FechaTerminoReal],
       ID_Unidad_Adm_Responsable
