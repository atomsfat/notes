SELECT [Descripcion_Corta] as ' '

  FROM [DM_PresupuestoN].[dbo].[Dim_Unidad_Administrativa] where ID_Unidad_Administrativa = ${region}

select Numero_Obra as 'Número Obra',
       Descripcion as Descripción
from [DM_Obras].[dbo].[Dim_Obra]
where ID_Obra = ${idobra}


select Num_Licitacion as 'Licitación',
       dta.Descripcion as 'Tipo de Adjudicación',
       dtl.Descripcion as 'Tipo de Licitación',
       '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(((Preciobase) / 1000000)  AS MONEY), 1)) as 'Precio Base MDP'
FROM [DM_Obras].[dbo].[Fac_Obras] fobr
     inner join [DM_Obras].[dbo].[Dim_Obra] dobr on fobr.ID_Obra = dobr.ID_Obra
     inner join [DM_Obras].[dbo].[Dim_Tipo_Adjudicacion] dta on fobr.ID_Tipo_Adjudicacion = dta.ID_Tipo_Adjudicacion
     inner join [DM_Obras].[dbo].[Dim_Tipo_Licitacion] dtl on fobr.ID_Tipo_Licitacion = dtl.ID_Tipo_Licitacion
     where fobr.ID_Obra = ${idobra}  and id_relacion = -1


select dcto.Cve_Contrato as 'Contrato',
       dtc.Descripcion as 'Tipo de contrato',
       '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(((IMP_Contrato + IMP_IVA_Contrato) / 1000000)  AS MONEY), 1)) as 'Importe Contrato MDP'


FROM [DM_Obras].[dbo].[Fac_Obras] fobr
     inner join [DM_Obras].[dbo].[Dim_Contrato] dcto on fobr.ID_Contrato = dcto.Id_Contrato and fobr.ID_Relacion = dcto.Id_Relacion
     inner join [DM_Obras].[dbo].[Dim_Tipo_Contratacion] dtc on fobr.ID_Tipo_Contratacion = dtc.Id_Tipo_Contratacion
      where fobr.ID_Obra = ${idobra}  and fobr.ID_Relacion = -1


select case when id_ciclo IS NULL then '2019' else ID_Ciclo end as 'Ejercicio Fiscal',
        right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((([IMP_Total])  / 1000000)  AS MONEY), 1)) + '     ', 25) as 'Presupuesto MDP',
        right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((([IMP_Pagado])  / 1000000)  AS MONEY), 1)) + '     ', 25) as 'Ejercido MDP',
        right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((([IMP_Asignacion] + IMP_IVA_Asignacion)  / 1000000)  AS MONEY), 1)) + '     ', 25) as 'Asignado MDP',
        [IMP_Meta] as'Meta KM'
FROM [DM_Obras].[dbo].[Fac_Obras] fobr
   where fobr.ID_Obra = ${idobra} and id_relacion = -1

select 'PEF - PRESUPESTO DE EGRESOS DE LA FEDERACION'  AS 'Esquema de financiamiento'
FROM [DM_Obras].[dbo].[Fac_Obras] fobr
  where id_obra = ${idobra}  and id_relacion = -1
