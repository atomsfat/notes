select

      case when DG.Descripcion = 'Infraestructura'        then 1
             when DG.Descripcion = 'Transporte'          then 2
             when DG.Descripcion = 'Comunicaciones'      then 3
             when DG.Descripcion = 'Puertos y Marina Mercante'   then 4
             when DG.Descripcion Is null   then 20
              end 'posicion',

      /* fac.Id_Fecha, */
      /* case when DG.Descripcion Is null then 'TOTAL' else '<h6>' + DG.Descripcion + '</h6>' end AS 'Gasto de Inversión',
           */
      case when DG.Descripcion Is null then 'TOTAL' else
      '<a data-toggle="collapse">

                    <p class="align-items">' +  DG.Descripcion +
                       '<b></b><b></b><b></b><b></b><b></b><b class="caret"></b>
                    </p>
                </a>'
         end AS 'Gasto de Inversión',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      cast(ROUND(((CAST(SUM((IMP_Contratado_Anual) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as '%',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual - IMP_Contratado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Por Comprometer',
      cast(ROUND(((CAST(SUM((IMP_Modificado_Anual - IMP_Contratado_Anual) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as '%',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Anual + IMP_Pagosiaff_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      cast(ROUND(((CAST(SUM((IMP_Ejercido_Anual + IMP_Pagosiaff_Anual) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as '%',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Anual - (IMP_Ejercido_Anual + IMP_Pagosiaff_Anual)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Por Pagar',
      cast(ROUND(((CAST(SUM((IMP_Contratado_Anual - (IMP_Ejercido_Anual + IMP_Pagosiaff_Anual)) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as '%'


  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino

  where fac.Id_Fecha =  REPLACE(${filterFecha},'-','')  and
  (fac.ID_DestinoGasto = 2 or
   fac.ID_DestinoGasto = 5 or
   fac.ID_DestinoGasto = 8 or
   fac.ID_DestinoGasto = 12
  )
    group by rollup(DG.Descripcion)
          order by 1


