SELECT
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end ' ',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

       right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS ' ' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS ' ',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS ' ',
      cast(ROUND(((CAST(SUM((IMP_Contratado_Anual) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as ' ',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual - IMP_Contratado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS ' ',
      cast(ROUND(((CAST(SUM((IMP_Modificado_Anual - IMP_Contratado_Anual) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as ' ',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Anual + IMP_Pagosiaff_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS ' ',
      cast(ROUND(((CAST(SUM((IMP_Ejercido_Anual + IMP_Pagosiaff_Anual) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as ' ',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Anual - (IMP_Ejercido_Anual + IMP_Pagosiaff_Anual)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS '  ',
      cast(ROUND(((CAST(SUM((IMP_Contratado_Anual - (IMP_Ejercido_Anual + IMP_Pagosiaff_Anual)) / 1000000)  AS MONEY)) /  (CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY))) *  100, 0) as int) as ' '



  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = REPLACE(${filterFecha},'-','')
          and (fac.ID_DestinoGasto = case when ${pProductLine} = 1 then 5
                                         when ${pProductLine} = 2 then 12
                                         when ${pProductLine} = 3 then 2
                                         when ${pProductLine} = 4 then 8
                                         when ${pProductLine} = 20 then 0 end)
  /* Carreteras Federales (12)
  ID_Unidad_Adm_Donante = '210' and not(fac.ID_Activ_AI = 010) and ID_Tipo_Gasto = 3 and
  not(fac.ID_Clasificacion_Gasto) like '85%' and ID_Tipo_Gasto >= 1 and not(fac.ID_Tipo_Gasto = 7)
  and fac.ID_ProgramaDestino <> '39'

*/


group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto
 /*
 union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
          and  and (fac.ID_DestinoGasto = ${pProductLine})

   /* Caminos Rurales (16) */
  ID_Unidad_Adm_Donante = '210' and ID_Activ_AI = 010 and ID_Subfuncion = 1 and
  (ID_Unidad_Trabajo = '031' or ID_Unidad_Trabajo = '037' or ID_Unidad_Trabajo = '039') and
   not(ID_Clasificacion_Gasto) like '85%' and not(ID_Unidad_Adm_Donante > '713') and
   ID_Tipo_Gasto >= 1 and not(ID_Tipo_Gasto = 7)

group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

 union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',


      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

   /* conservacion (17) */
  ID_Unidad_Adm_Donante = '211' and ID_Subfuncion = 01 and ID_Activ_AI = 003 and
  ID_Unidad_Trabajo = '032' and ID_Tipo_Gasto >= 1 and not(ID_Tipo_Gasto = 7)

group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

 union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

  /* R26 Provisiones para el desarrollo, modern (19)  */
  (ID_Unidad_Adm_Donante = '211' and ID_Subfuncion = 01 and ID_Activ_AI = 003 and ID_Activ_AP like 'R0026%' and ID_Entidad_Federativa = 34 and
   not(ID_Clasificacion_Gasto) like '85%'and ID_Tipo_Gasto >= 1
  )

   group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

 /* APPs (18)  */
  ID_Subfuncion = 1 and ID_Activ_AI = 003 and ID_Activ_AP = 'G0003'and
  (ID_Clasificacion_Gasto = 33904 or ID_Clasificacion_Gasto = 33104) and
  ID_Tipo_Gasto >= 1 and not(ID_Unidad_Adm_Responsable = '641') and not(ID_Tipo_Gasto = 7) and
  not(ID_Clasificacion_Gasto) like '85%' and not(ID_Unidad_Adm_Donante > '713')

   group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

/* PPS's (20) */
  ID_Unidad_Adm_Donante = '214' and ID_Activ_AI = 003 and ID_Subfuncion = 1 and
  ID_Clasificacion_Gasto = '33902' and ID_Tipo_Gasto = 2 and not(ID_Clasificacion_Gasto like '85%') and
  not(ID_Unidad_Adm_Donante > '713') and ID_Tipo_Gasto >= 1 and not(ID_Tipo_Gasto = 7)

   group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

/* Otros de Infraestructura 21 */
  ((ID_Unidad_Adm_Donante = '212' or ID_Unidad_Adm_Donante = '214') and
  (ID_Activ_AI = 003 or ID_Activ_AI = 8) and ID_Subfuncion = 1 and
  not(ID_Clasificacion_Gasto like '85%') and not(ID_Unidad_Adm_Donante > '713')  and
  not(ID_Activ_AI = 3 and ID_Activ_AP = 'U0002' and ID_Clasificacion_Gasto = '43801' and ID_Tipo_Gasto = 3)
  and ID_Tipo_Gasto >= 1 and not(ID_Tipo_Gasto = 7) and not(ID_Clasificacion_Gasto = '33902'))

   group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

 union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo, modern'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

/* Gasto reasignado (25) */
  ID_Unidad_Adm_Donante like '2%' and ID_Clasificacion_Gasto like '85%' and
  ID_Tipo_Gasto >= 1 and not(ID_Tipo_Gasto = 7)

   group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto

 union select
  case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5 then 1   /*'Carreteras Federales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4 then 2   /*'Caminos Rurales y Alimentadores'*/
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6 then 3   /*'Conservación de Carreteras' */
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 4 /*'R26 Provisiones para el desarrollo, modern (19)' */
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 5  /*'APP´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 6  /*'PPS´s'*/
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 7  /*'Otros infraestructura'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 8  /*'Gasto reasignado a gobiernos estatales'*/
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 9  /*'Programa de apoyo en infraestructura'*/
       end 'pos',
      case when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 5  then 'Carreteras Federales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 4  then 'Caminos Rurales y Alimentadores'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'Conservación de Carreteras'
           when ID_Unidad_Adm_Donante = 211 and fac.ID_ProgramaDestino = 6  then 'R26 Provisiones para el desarrollo'
           when ID_Unidad_Adm_Donante = 212 and fac.ID_ProgramaDestino = 18 then 'APP´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 20 then 'PPS´s'
           when ID_Unidad_Adm_Donante = 214 and fac.ID_ProgramaDestino = 18 then 'Otros infraestructura'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 30 then 'Gasto reasignado a gobiernos estatales'
           when ID_Unidad_Adm_Donante = 210 and fac.ID_ProgramaDestino = 39 then 'Programa de apoyo en infraestructura'
           else  PD.Descripcion end '  ',

      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM(IMP_Original_Anual / 1000000)  AS MONEY), 1)) + '     ', 25)                                     AS 'Original' ,
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Modificado_Anual) / 1000000)  AS MONEY), 1)) + '     ', 25) AS 'Modificado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)    AS 'Comprometido',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Pagado',
      right('$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SUM((IMP_Contratado_Al_Mes - (IMP_Ejercido_Al_Mes + IMP_Pagosiaff_Al_Mes)) / 1000000)  AS MONEY), 1)) + '     ', 25)     AS 'Disponible'

  FROM [DM_PresupuestoN].[dbo].[Fac_Presupuesto] fac
  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_DestinoGasto] DG on fac.ID_DestinoGasto = DG.ID_DestinoGasto

  INNER JOIN [DM_PresupuestoN].[dbo].[Dim_ProgramaDestino] PD on fac.[ID_ProgramaDestino] = PD.ID_ProgramaDestino


  where
         fac.Id_Fecha = 20191001
           and (fac.ID_DestinoGasto = ${pProductLine})

   /*Programa de apoyo en infraestructura carretera (36) */
  ((ID_Unidad_Adm_Responsable = '624' or ID_Unidad_Adm_Responsable = '632') and
  ID_Activ_AI = 3 and ID_Activ_AP = 'U0002' and ID_Clasificacion_Gasto = '43801' and
  ID_Tipo_Gasto >= 1)

   group by fac.ID_Fuente_Fin,
           DG.Descripcion,
           PD.Descripcion,
           ID_Unidad_Adm_Donante,
           fac.ID_ProgramaDestino,
           fac.ID_DestinoGasto
 */
   order by 1, 2

